package javaTesting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

// test case refers to @test/ @before @after is the requirement to access the test

//	@beforeClass
//		@before
//		@test
//		@after
//	@AfterClass


//test scenario:	ATM Application DWC
//		test case 1	@test solve each test case
//		test case`2 based on Each scenario
//		test case 3 Meaning different steps
//		test case 4
//run test suite:
//	Execute all test Case

public class Javatesting2 {
	
	static ATMImplement ref;
	
	@BeforeClass
	public static void testCase() {
		ref= new ATMImplement();
		ref.userChoice(3);
		assertTrue(ref.checkBalance()==0);
	}
	
	@Before
	public void testcase2() {
		System.out.println("After Deposit available Balance amount:"+String.format("%.2f",ref.deposit(20.50)));
		
		
	}
	@Test
	public void testCase3() {
		ref.userChoice(3);
		
	}
	@After
	public void testcase4() {
		System.out.println("After withdrawing: 15.20\n"+ref.withdrawal(15.20));
	}
	@AfterClass
	public static void testcase5() {
		ref.userChoice(3);
		assertEquals("The balance should be greater than 6", true, ref.checkBalance()>5);
	}
	
	
}

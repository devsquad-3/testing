package javaTesting;

import java.util.Scanner;

public class ATMImplement implements ATM{
	Scanner refScanner=null;
	double availableBalance=0;

	@Override
	public double deposit(double amount) {
		availableBalance+=amount;
		return availableBalance;
	}

	@Override
	public double withdrawal(double amount) {
		availableBalance-=amount;
		return availableBalance;
	}

	@Override
	public double checkBalance() {
		// TODO Auto-generated method stub
		return availableBalance;
	}
	void userInput() {
		
		refScanner=new Scanner(System.in);
		int choice=refScanner.nextInt();
		userChoice(choice);
		System.out.println("Do u wish to continue?");
		
	}
	
	void userChoice(int choice) {
		switch (choice) {
		case 1:
			System.out.println("Enter Amount for deposit:");
			var deposit=refScanner.nextDouble();
			deposit(deposit);
			break;
		case 2:
			System.out.println("Enter Amount for withdraw:");
			var withdraw=refScanner.nextDouble();
			withdrawal(withdraw);
			break;
		case 3:
			System.out.println("Your Balance:");
			System.out.println(String.format("%.2f", checkBalance()));
			break;
		default:
			break;
		}
	}

}

package javaTesting;

public interface ATM {
	double deposit(double amount);
	double withdrawal(double amount);
	double checkBalance();
	
}
